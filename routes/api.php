<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//TICKETS

Route::get('tickets/{ticket}/messages', 'TicketController@getMessagesByTicket');
Route::get('tickets/{ticket}/participants', 'TicketController@getUsersByTicket');

Route::get('tickets/{ticket}/files', 'TicketController@getFilesByTicket');

Route::post('tickets/{ticket}/participants', 'TicketController@addParticipants');

Route::get('files/{file}', 'FileController@downloadFile');

Route::get('tickets/user/{user}', 'TicketController@getTicketsByUser');
Route::get('tickets/user/{user}/admin', 'TicketController@getTicketsByUserAdmin');

//edit service and subject
Route::put('tk/service/subject', 'TicketController@updateTkServiceSubject');
//escalar ticket
Route::put('tk/escalate', 'TicketController@escalateTicket');
//close ticket
Route::put('tk/status', 'TicketController@closeTicket');

// INDICADORES
	// Productividad
Route::get('tk/indicators/productivity', 'IndicatorController@getTicketsProductivity');
	// Por servicios
Route::get('tk/indicators/services', 'IndicatorController@getTicketsByServices');
	// Por subjects
Route::get('tk/indicators/subjects', 'IndicatorController@getTicketsBySubjects');
	// Por Department
Route::get('tk/indicators/departments', 'IndicatorController@getTicketsByDepartments');
	// Por attention time
Route::get('tk/indicators/attentionTime', 'IndicatorController@getAttentionTime');

//DEPARTAMENTOS
Route::get('departments/admins', 'DepartmentController@getDepartmentsAdmins');
// Route::get('departments/{department}/tickets', 'DepartmentController@getTickets');

//SERVICIOS
Route::get('services/department/{department_id}', 'ServiceController@getServicesByDepartment');

//ASUNTOS
Route::get('subjects/service/{service_id}', 'SubjectController@getSubjectsByService');
