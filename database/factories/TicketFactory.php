<?php

use Faker\Generator as Faker;

$factory->define(App\Ticket::class, function (Faker $faker) {
    return [
         'subject' => $faker->sentence(8, false)
    ];
});
