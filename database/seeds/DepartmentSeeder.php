<?php
use App\Department;
use Illuminate\Database\Seeder;
// use Illuminate\Support\Facades\DB;


class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Department::create([
            'name' => 'Dirección',
            'email' => 'ammmec@ammmec.com',
            'phone' => '+52 1 (493) 103 0084',
        ]);
         
    	Department::create([
    		'name' => 'Sistemas',
            'email' => 'sistemas@ammmec.com',
            'phone' => '+52 1 (493) 100 8684',
    	]);

    	Department::create([
    		'name' => 'Finanzas',
            'email' => 'finanzas_fresnillo@ammmec.com',
            'phone' => '+52 1 (493) 115 7255',
    	]);

    	Department::create([
    		'name' => 'Compras',
            'email' => 'compras_fresnillo@ammmec.com',
            'phone' => '+52 1 (493) 111 1184',
    	]);

    	Department::create([
    		'name' => 'Conservación',
            'email' => 'conservacion_fresnillo@ammmec.com',
            'phone' => '+52 1 (493) 114 0635',
    	]);

        Department::create([
            'name' => 'Suministros',
            'email' => 'suministros_fresnillo@ammmec.com',
            'phone' => '+52 1 (493) 112 8455',
        ]);

    	Department::create([
    		'name' => 'Ventas',
            'email' => 'ventas_fresnillo@ammmec.com',
            'phone' => '+52 1 (493) 106 9062',
    	]);

    	Department::create([
    		'name' => 'Operaciones',
            'email' => 'planeacion_fresnillo@ammmec.com',
            'phone' => '+52 1 (493) 106 9062',
    	]);

        Department::create([
            'name' => 'Recursos Humanos',
            'email' => 'recursos_humanos@ammmec.com',
            'phone' => '+52 1 (493) 114 6223',
        ]);

        Department::create([
            'name' => 'Seguridad',
            'email' => 'seguridad_fresnillo@ammmec.com',
            'phone' => '+52 1 (493) 102 6056',
        ]);

        Department::create([
            'name' => 'Normatividad',
            'email' => 'guillermo_murillo@ammmec.com',
            'phone' => '+52 1 (493) 102 6056',
        ]);

        Department::create([
            'name' => 'Mercadotecnia',
            'email' => 'mercadotecnia_fresnillo@ammmec.com',
            'phone' => '+52 1 (614) 137 8964',
        ]);

        Department::create([
            'name' => 'Almacén',
            'email' => 'almacen_fresnillo@ammmec.com',
            'phone' => '+52 1 (493) 00 0000',
        ]);

        Department::create([
            'name' => 'Smart Condition',
            'email' => 'ventas@smartcondition.mx',
            'phone' => '+52 1 (493) 114 1974',
        ]);
    }
}
