<?php
use App\Service;
use App\Department;
use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$departmentId = Department::where('name', 'Sistemas')->value('id');

        Service::create([
    		'name' => 'Servicio de correo electrónico',
            'status' => 'Activo',
            'description' => 'Toda situación relacionada con el correo electrónico entran en esta categoria',
    		'department_id' => $departmentId,
    	]);

    	// Service::create([
    	// 	'name' => 'Mantenimiento de computadoras',
     //        'status' => 'Activo',
    	// 	'department_id' => $departmentId,
    	// ]);

    	// Service::create([
    	// 	'name' => 'Desarrollo de software',
     //        'status' => 'Inactivo',
    	// 	'department_id' => $departmentId,
    	// ]);

     //    $departmentId = Department::where('name', 'Finanzas')->value('id');

     //    Service::create([
     //        'name' => 'Contabilidad',
     //        'status' => 'Activo',
     //        'department_id' => $departmentId,
     //    ]);

     //    Service::create([
     //        'name' => 'Comprobante de gasto',
     //        'status' => 'Activo',
     //        'department_id' => $departmentId,
     //    ]);

     //    Service::create([
     //        'name' => 'Facturación',
     //        'status' => 'Inactivo',
     //        'department_id' => $departmentId,
     //    ]);
    }
}
