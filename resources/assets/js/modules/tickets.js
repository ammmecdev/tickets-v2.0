export default{
	namespaced: true,
	state:{
		messages: [],
        tickets: [],
        ticketActive:null,
        listTickets: [],
        data_status: {
            title: null,
            question: null,
            status: null,
        },
        listParticipants: [],
        listFiles: [],
        listUsers: [],
        isBussyParticipants: false,
        isBussyFiles: false,
        isBussyUsers: false,
        listUsersOnline: []
	},
	mutations:{
		addMessage(state, message)
        {
            state.messages.push(message) 
        },
        updateTicket(state, ticket)
        {
            const ticket_state = state.listTickets.find(function(ticket_state){
                    return ticket_state.id == ticket.id
            })
            if(ticket_state){
                ticket_state.updated_at = ticket.updated_at
                ticket_state.status = ticket.status
                ticket_state.status_admin = ticket.status_admin
            }
        },
        setMessages(state, messages)
        {
            state.messages = messages
        },
        addTicket(state, ticket)
        {
            state.listTickets.push(ticket) 
        },
        emptyListTickets(state, list)
        {
            state.listTickets = []
        },
        addTickets(state, tickets)
        {
            state.listTickets.push(...tickets)
        },
        setTicket(state, ticket)
        {   
            state.ticketActive = ticket
        },
        setDataStatus(state, data)
        {
            state.data_status.title = data.title
            state.data_status.question = data.question
            state.data_status.status = data.status
        },
        setListParticipants(state, participants)
        {
            state.listParticipants = participants

            const user_ticket = {
                id: state.ticketActive.user_id, 
                name: state.ticketActive.user_name,
                picture: state.ticketActive.user_picture, 
                department_id: state.ticketActive.department_id,
                online: false
            }

            state.listParticipants.push(user_ticket)
        },
        setListUsers(state, users)
        {
            state.listUsers = users
        },
        setListFiles(state, files)
        {
            state.listFiles = files
        },
        addMessageUnread(state, ticket_id)
        {
            const ticket_state = state.listTickets.find(function(ticket_state){
                    return ticket_state.id == ticket_id
            })
            if(ticket_state)
                ticket_state.messages_unread = ticket_state.messages_unread + 1
        },
        resetMessageUnread(state, ticket_id)
        {
            const ticket_state = state.listTickets.find(function(ticket_state){
                    return ticket_state.id == ticket_id
            })
            if(ticket_state)
                ticket_state.messages_unread = 0
        },
        setUserOnline(state, user){
            state.listUsersOnline.push(user);
            
            const participant_state = state.listParticipants.find(function(participant_state){
                    return participant_state.id == user.id
            })

            if(participant_state)
                participant_state.online = true
        },
        removeUserOffline(state, user)
        {
            const index = state.listUsersOnline.findIndex((user_state) => {
                return user_state.id == user.id;
            });
            state.listUsersOnline.splice(index, 1);

            const participant_state = state.listParticipants.find(function(participant_state){
                    return participant_state.id == user.id
            })
            if(participant_state)
                participant_state.online = false
        },
        testParticipantsHere(state)
        {
            state.listParticipants.forEach((participant) => {
                const user_online = state.listUsersOnline.find(function(user){
                    return user.id == participant.id
                })
                if(user_online){
                    participant.online = true
                }
            });
        },
        changeIsBussyParticipants(state)
        {
            state.isBussyParticipants = !state.isBussyParticipants
        },
        changeIsBussyFiles(state)
        {
            state.isBussyFiles = !state.isBussyFiles
        },
        changeIsBussyUsers(state)
        {
            state.isBussyUsers = !state.isBussyUsers
        },
        addNewParticipants(state, newParticipants)
        {
            state.listParticipants = state.listParticipants.concat(newParticipants);
        },
	},
	actions:{
        getMessages(context, ticket_id)
        {
            axios.get(BaseUrl + `/api/tickets/${ticket_id}/messages`)
            .then(response => {
                context.commit('setMessages', response.data)
                context.dispatch('deleteMessagesUnread', ticket_id)
            });
        },
        getParticipants(context, ticket_id)
        {
            context.commit('changeIsBussyParticipants')
            axios.get(BaseUrl + `/api/tickets/${ticket_id}/participants`)
            .then(response => {
                context.commit('setListParticipants', response.data)
                context.commit('changeIsBussyParticipants')
                context.commit('testParticipantsHere')
            });
        },
        getFiles(context, ticket_id)
        {
            context.commit('changeIsBussyFiles')
            axios.get(BaseUrl + `/api/tickets/${ticket_id}/files`)
            .then(response => {
                context.commit('setListFiles', response.data)
                context.commit('changeIsBussyFiles')
            });
        },
        getUsers(context)
        {
            context.commit('changeIsBussyUsers')
            axios.get(BaseUrl + `/api/users/all`,  
                {params: {listParticipants: context.state.listParticipants}}
                ).then(response=>{
                context.commit('setListUsers', response.data)
                context.commit('changeIsBussyUsers')
            });
        },
        deleteMessagesUnread(context, ticket_id)
        {
            axios.put(BaseUrl + `/api/tickets/${ticket_id}/messages/unread`);
            context.commit('resetMessageUnread', ticket_id)
        },
	},
	getters:{
		
	}
}