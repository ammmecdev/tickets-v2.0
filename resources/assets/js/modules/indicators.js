export default{
	namespaced: true,

	state:{
		productivity: [],
		tkservices: [],
		tksubjects: [],
		tkdepartment: [],
        tkattentionTime: [], 
        dateStart: null,
        dateEnd: null,
	},
	mutations:{
		setProductivity(state, productivity)
        {
            state.productivity = productivity
        },
        setByServices(state, tkservices)
        {
        	state.tkservices = tkservices
        },
        setBySubjects(state, tksubjects)
        {
        	state.tksubjects = tksubjects
        },
        setByDepartments(state, tkdepartment)
        {
        	state.tkdepartment = tkdepartment
        },
        setAttentionTime(state, tkattentionTime)
        {
            state.tkattentionTime = tkattentionTime
        },
        setRangeDate(state, date)
        {
            state.dateStart = date.start
            state.dateEnd = date.end
        }
	},
	actions:{
		getTicketsProductivity(context, department_id){
            axios.get(BaseUrl + `/api/tk/indicators/productivity`, {
                params: {
                  start: context.state.dateStart,
                  end: context.state.dateEnd,
                  department_id: department_id
                }
            })
            .then(response => {
                context.commit('setProductivity', response.data)
            });
        },
        getTicketsByService(context, department_id){
            axios.get(BaseUrl + `/api/tk/indicators/services`, {
                params: {
                  start: context.state.dateStart,
                  end: context.state.dateEnd,
                  department_id: department_id
                }
            })
            .then(response => {
                context.commit('setByServices', response.data)
            });
        },
        getTicketsBySubject(context, department_id){
            axios.get(BaseUrl + `/api/tk/indicators/subjects`, {
                params: {
                  start: context.state.dateStart,
                  end: context.state.dateEnd,
                  department_id: department_id
                }
            })
            .then(response => {
                context.commit('setBySubjects', response.data)
            });
        },
        getTicketsByDepartment(context, department_id){
            axios.get(BaseUrl + `/api/tk/indicators/departments`, {
                params: {
                  start: context.state.dateStart,
                  end: context.state.dateEnd,
                  department_id: department_id
                }
            })
            .then(response => {
                context.commit('setByDepartments', response.data)
            });
        },
        getTicketsAttentionTime(context, department_id){
            axios.get(BaseUrl + `/api/tk/indicators/attentionTime`, {
                params: {
                  start: context.state.dateStart,
                  end: context.state.dateEnd,
                  department_id: department_id
                }
            })
            .then(response => {
                context.commit('setAttentionTime', response.data)
            });
        }
	},
	getters:{
		
	}
}