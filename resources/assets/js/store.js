import Vue from 'vue';
import Vuex from 'vuex';
import indicators from './modules/indicators'
import tickets from './modules/tickets'
import notifications from './modules/notifications'


Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		user: null,
        isAdmin: null,
        subscription: null,
        urlShowImage: '',
    },
    mutations: {
        setUser(state, user){
            state.user = user
        },
        setIsAdmin(state, isAdmin){
            state.isAdmin = isAdmin
        },
        addSubcription(state, subscription){
            state.subscription = subscription
        },
        setUrlShowImage(state, url)
        {
            state.urlShowImage = url
        },
    },
    actions: {
        
    },
    getters: {

    },
    modules:{
        tickets,
        indicators,
        notifications
    }
});