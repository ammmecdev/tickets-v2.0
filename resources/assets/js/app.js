require('./bootstrap');

import Vue from 'vue'
import VueGoogleCharts from 'vue-google-charts'
import router from './router'
import store from './store'
import VueFormWizard from 'vue-form-wizard'
import InfiniteLoading from 'vue-infinite-loading';
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
import VCalendar from 'v-calendar'
import CKEditor from '@ckeditor/ckeditor5-vue';

window.BaseUrl = '';
// window.BaseUrl = '/tickets-v2.0/public';

Vue.use(VCalendar);
Vue.use(VueGoogleCharts);
Vue.use(VueFormWizard);
Vue.use(InfiniteLoading);
Vue.use(CKEditor);


Vue.component('navbar-component', require('./components/NavbarComponent.vue'));

Vue.component('modal-show-image-component', require('./components/tickets/ModalShowImageComponent.vue'));
Vue.component('message-image-component', require('./components/tickets/MessageImageComponent.vue'));

Vue.component('show-ticket', require('./components/tickets/ShowTicket.vue'));
Vue.component('info-ticket-component', require('./components/tickets/InfoTicketComponent.vue'));
Vue.component('message-component', require('./components/tickets/MessageComponent.vue'));
Vue.component('list-tickets-component', require('./components/tickets/ListTicketsComponent.vue'));
Vue.component('form-message-component', require('./components/tickets/FormMessageComponent.vue'));
Vue.component('form-report-component', require('./components/tickets/FormReportComponent.vue'));

Vue.component('no-ticket-active-component', require('./components/tickets/NoTicketActiveComponent.vue'));
Vue.component('form-add-participant-component', require('./components/tickets/FormAddParticipantComponent.vue'));
Vue.component('picture-user-for-participant-component', require('./components/tickets/PictureUserForParticipantComponent.vue'));

Vue.component('ticket-active-component', require('./components/tickets/TicketActiveComponent.vue'));
Vue.component('ticket-component', require('./components/tickets/TicketComponent.vue'));
Vue.component('form-new-ticket-component', require('./components/tickets/FormNewTicketComponent.vue'));
Vue.component('form-edit-tservice-component', require('./components/tickets/FormEditTServiceComponent.vue'));
Vue.component('form-escalate-ticket-component', require('./components/tickets/FormEscalateTicketComponent.vue'));
Vue.component('form-status-ticket-component', require('./components/tickets/FormStatusTicketComponent.vue'));
Vue.component('img-no-messages-component', require('./components/tickets/ImgNoMessages.vue'));
Vue.component('list-participants-component', require('./components/tickets/ListParticipantsComponent.vue'));
Vue.component('participant-component', require('./components/tickets/ParticipantComponent.vue'));
Vue.component('list-files-component', require('./components/tickets/ListFilesComponent.vue'));
Vue.component('file-component', require('./components/tickets/FileComponent.vue'));

// INDICADORES
Vue.component('indicadores-tickets', require('./components/indicators/Indicadores.vue'));
Vue.component('indicadores-productividad', require('./components/indicators/Productividad.vue'));
Vue.component('indicadores-tiempo-solucion', require('./components/indicators/TiempoSolucion.vue'));
Vue.component('indicadores-tickets-por-servicio', require('./components/indicators/TicketsPorServicio.vue'));
Vue.component('indicadores-tickets-por-asunto', require('./components/indicators/TicketsPorAsunto.vue'));
Vue.component('indicadores-tickets-por-departamento', require('./components/indicators/TicketsPorDepartment.vue'));
Vue.component('indicadores-promedio', require('./components/indicators/PromedioPorDias.vue'));


//NOTIFICATIONS
Vue.component('notifications', require('./components/notifications/Notifications.vue'))
Vue.component('notification-component', require('./components/notifications/NotificationComponent.vue'))
Vue.component('notifications-list-component', require('./components/notifications/ListNotificationsComponent.vue'))


Vue.component('notifications-all-component', require('./components/notifications/AllNotificationsComponent.vue'))



const app = new Vue({
    el: '#app',
    store,
    router
});
