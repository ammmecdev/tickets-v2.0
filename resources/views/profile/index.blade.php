@extends('layout')
@section('content')

<indicadores-tickets :user="{{ Auth::user() }}"/>

@endsection
@section('script')
@routes
<script type="text/javascript">
	$(document).ready(function(){
		$('.info').popover('show');
		$('.info').popover('toggle');
	});
</script>
@endsection