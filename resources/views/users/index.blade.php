@extends('layout')
@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-3 col-lg-3 mt-1">
			<ul class="list-group buttons">
				<li class="list-group-item bg-light">
					<h1 class="display-1 mt-2" style="font-size: 25px">Ver</h1>
				</li>
				<li class="list-group-item">
					<input id="radiobtn_1" class="radiobtn" name="filter" type="radio" value="All" tabindex="1" checked>
					<span></span>
					<label for="radiobtn_1">Todos</label>
				</li>

				<li class="list-group-item">
					<input id="radiobtn_2" class="radiobtn" name="filter" type="radio" value="Admin" tabindex="1">
					<span></span>
					<label for="radiobtn_2">Administradores</label>
				</li>

				<li class="list-group-item">
					<input id="radiobtn_3" class="radiobtn" name="filter" type="radio" value="Regular" tabindex="1">
					<span></span>
					<label for="radiobtn_3">Regulares</label>
				</li>
			</ul>	
		</div>
		<div role="main" class="col-md-9 col-lg-9 px-2 contentCruds">
			<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-2">
				<h1 class="display-4 text-danger" style="font-size: 40px;">Usuarios</h1>
			</div>

			@if($users->isNotEmpty())
			<table class="table table-hover" id="table-users" style="width: 100%;">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Usuario</th>
						<th scope="col">Email</th>
						<th scope="col">Admin</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
				<tfoot>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Usuario</th>
						<th scope="col">Email</th>
						<th scope="col">Admin</th>
					</tr>
				</tfoot>
			</table>
			@else
			<div class="row justify-content-center">
				<div>
					<img src="{{ asset('images/sintickets.png') }}" alt="" class="center-block imag">
				</div>
			</div>
			@endif
		</div>
	</div>
</div>
@endsection
@section('script')
@routes
<script type="text/javascript">
	filter = "All";

	$(document).ready(function() {
		table(filter);

		$('.radiobtn').click(function() {
			filter = $('input:radio[name=filter]:checked').val();
			table(filter);
		});

	});

	function table(filter)
	{
		var table = $('#table-users').DataTable( {
			paging:   true,
			ordering: true,
			info:     true,
			filter:   true,
			processing: true,
			serverSide: true,
			destroy: true,
			scrollX: true,
			order: [[ 0, "asc" ]],
			language: {
				lengthMenu: "Mostrar _MENU_ registros por página",
				zeroRecords: "No se encontró ningún registro",
				info: "Mostrando página _PAGE_ de _PAGES_",
				infoEmpty: "No hay registros disponibles",
				infoFiltered: "(Filtrado de _MAX_ registros)",
				search: "Buscar:",
				processing: "Procesando...",
				paginate: {
					first:      "Inicio",
					last:       "Fin",
					next:       "Siguiente",
					previous:   "Anterior"
				},
			},
			ajax:  {
				url: route('users.getUsers', [filter])
			},
			columns: [
			{ data: 'id', name: 'id' },
			{ data: 'name', name: 'name' },
			{ data: 'email', name: 'email' },
			{ data: 'is_admin', name: 'is_admin' }
			],
			search: {
				"regex": true
			}
		});
	}

	function changeTypeUser(id, type) {
		$.ajax({ 
			method: 'POST',
			url:  '{{ url('/usuarios/updateType') }}', 
			data: {
				"_token": "{{ csrf_token() }}",
				"id": id,
				"type": type
			}, 
			success: function(data){ 
				table(filter);
				$('.font-message').html('Se ha actualizado correctamente el tipo de usuario');
				$('#div-success').show();
				setTimeout(function() {
					$("#div-success").fadeOut(3500);
				},5000);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$('.font-message').html('Ha ocurrido un error al actualizar el tipo de usuario');
				$('#div-error').show();
				setTimeout(function() {
					$("#div-error").fadeOut(3500);
				},5000);
			}
		});
	} 
</script>
@endsection