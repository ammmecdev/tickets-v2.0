 <!doctype html>
 <html lang="es">
 <head>
 	<meta charset="utf-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 	<meta name="description" content="">
 	<meta name="author" content="">
 	<meta name="csrf-token" content="{{ csrf_token() }}">

 	<link rel="icon" href="{{ asset('images/logo/logoammmec.png') }}">

 	<title>TICKETS AMMMEC</title>

 	<!-- Bootstrap core CSS -->
 	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

 	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" rel="stylesheet">

 	<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
{{-- 
 	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"> 	 --}}

 	<link href="{{ asset('css/dashboard/dashboard.css') }}" rel="stylesheet">

 	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

 	{{-- <link href="{{ asset('chat/template.min.css') }}" rel="stylesheet"> --}}
	

{{-- 	<link rel="stylesheet" href="{{ asset('css/tickets/tickets.css') }}">

	<link rel="stylesheet" href="{{ asset('css/profile/profile.css') }}">
 --}}

 	{{-- ckeditor --}}
 	{{-- <link href="{{ asset('plugins/ckeditor/samples.css') }}" rel="stylesheet"> --}}
 	
 	<!-- Bootstrap core JS -->
 	<script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
 	{{-- <script src="{{ asset('chat/jquery.min.js') }}"></script>
 	<script src="{{ asset('chat/bootstrap.bundle.min.js') }}"></script>
 	<script src="{{ asset('chat/plugins.bundle.js') }}"></script>
 	<script src="{{ asset('chat/template.js') }}"></script>
 	 --}}
 	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
 	
 	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

 	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

 	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

 	{{-- <script src="{{ asset('js/select2.min.js') }}"></script> --}}

 	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

{{-- 
 	<script src="{{ asset('plugins/ckeditor/ckeditor.js') }}"></script> --}}

 	<style>
 	.page-item.active .page-link {
 		z-index: 1;
 		color: #fff;
 		background-color: #dc3545;
 		border-color: #dc3545;
 	}

 	.custom-control-input:checked~.custom-control-label::before {
 		color: #fff;
 		border-color: #dc3545;
 		background-color: #dc3545;
 	}
 	.page-link{
 		color:#18191b;
 	}

 	.radio-filter{
 		background-color: #dc3545;
 		color:#dc3545;
 	}

 	.page-item.disabled .page-link{
 		color: #aaa;
 	}

 	[data-href] {
 		cursor: pointer;
 	}

 	/* Radio button */
 	.radiobtn {
 		display: none;
 	}

 	.buttons li {
 		display: block;
 	}
 	.buttons li label{
 		padding-left: 30px;
 		position: relative;
 		left: -25px;
 	}
 	.buttons li label:hover {
 		cursor: pointer;
 	}
 	.buttons li span {
 		display: inline-block;
 		position: relative;
 		top: 5px;
 		border: 2px solid #ccc;
 		width: 18px;
 		height: 18px;
 		background: #fff;
 	}
 	.radiobtn:checked + span::before{
 		content: '';
 		border: 2px solid #fff;
 		position: absolute;
 		width: 14px;
 		height: 14px;
 		background-color: rgb(227, 27, 35) !important;
 	}
 	/*.active{
 		background-color: #dc3545 !important;
 	}*/

 	.alert-bottom{
 		position: fixed;
 		z-index: 3;
 		bottom: 0px;
 		left:1%;
 		width: 98%;
 	}

 	.rounded-circle{
 		min-width: 40px; 
 		min-height: 40px; 
 		max-width: 40px; 
 		max-height: 40px;
 	}

 	.dropdown-item:active {
 		/*background-color: rgb(227, 27, 35) !important;*/
 		background-color: rgb(106, 115, 123) !important;
 	}

	.btn-danger{
		background-color: rgb(227, 27, 35) !important;
	}

	.bg-danger-15{
		background-color: rgb(227, 27, 35, .15) !important;
		color: rgb(69, 85, 95) !important;
	}

	.bg-danger-35{
		background-color: rgb(227, 27, 35, .35) !important;
		color: rgb(69, 85, 95) !important;
	}

	.bg-light{
		background-color: rgb(159, 161, 164, .1) !important;
		color: rgb(69, 85, 95) !important;
	}

	.bg-light-8{
		background-color: rgb(159, 161, 164, .8) !important;
		color: rgb(69, 85, 95) !important;
		opacity: 8%;
	}

	.bg-light-15{
		background-color: rgb(159, 161, 164, .15) !important;
		color: rgb(69, 85, 95) !important;
	}

	.bg-light-35{
		background-color: rgb(159, 161, 164, .35) !important;
		color: rgb(69, 85, 95) !important;
	}

	.bg-dark{
		background-color: rgb(69, 85, 95) !important;
	}

	.bg-secondary{
		background-color: rgb(106, 115, 123) !important;
	}

	.alert-secondary{
		background-color: rgb(106, 115, 123, .20) !important;
		color: rgb(69, 85, 95) !important;
	}

	.bg-white{
		background-color: white;
	}

	.navbar-dark{
		background-color: rgb(69, 85, 95) !important;
	}
	
	.text-dark{
		color: rgb(69, 85, 95) !important;
	}

	.text-dark-80{
		color: rgb(69, 85, 95, .80) !important;
	}

	.text-secondary{
		color: rgb(106, 115, 123) !important;
	}

	.text-secondary-light{
		color: rgb(106, 115, 123, .70) !important;
	}

	.danger{
		color: rgb(227, 27, 35) !important;
	}

	h1, h2, h3, h4, h5, p, {
		color: rgb(69, 85, 95) !important;
	}

	.ck.ck-editor__main>.ck-editor__editable:not(.ck-focused) {
        border-color: rgb(159, 161, 164, .35) !important;
    }

    .ck.ck-editor__main {
        margin-top: -2px !important;
    }

    .ck.ck-toolbar {
         border: 1px solid rgb(159, 161, 164, .35) !important; 
    }

    .ck.ck-editor__editable:not(.ck-editor__nested-editable).ck-focused {
    	/* outline: none; */
    	border-color: rgb(159, 161, 164, .35) !important;
    }

	/*body {
  font-family: Calibri; 
} */

 </style>
</head>

<body>
	<div id="app">
		<div class="tab-content" id="v-pills-tabContent">
			<navbar-component :user="{{ auth()->user() }}" />
		</div>
		<div class="tab-content" id="v-pills-tabContent" style="margin-top: 75px;">
			@yield('content')
		</div>

	</div>

	@yield('script')
	<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
