@extends('layout')
@section('content')

<link rel="stylesheet" href="{{ asset('css/tickets/show.css') }}"> 

<show-ticket :ticket="{{ $ticket }}" :messages="{{ $messages }}" :user="{{ Auth::user() }}" :is-admin="{{ $isAdmin }}"/>

@endsection
