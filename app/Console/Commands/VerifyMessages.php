<?php

namespace App\Console\Commands;

use App\User;
use App\TicketUser;
use Illuminate\Console\Command;
use App\Notifications\UnreadMessages;

class VerifyMessages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verify:messages';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Revisa los mensajes no leidos y le notifica al usuario que tiene mensajes pendientes por antender por correo electronico';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();
        foreach($users as $user){
            $messages_unread = TicketUser::where('user_id', $user->id)->where('messages_unread', '>', 0)->get();
            if($messages_unread->count() > 0){
                 $user->notify(new UnreadMessages());
            }
        }
    }
}
