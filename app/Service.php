<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
	protected $guarded = ['id'];

	protected $fillable = [
		'name', 'status', 'description', 'department_id', 
	];

	public function tickets(){
		return $this->hasMany(Ticket::class);
	} 

	public function subjects(){
		return $this->hasMany(Subject::class);
	}

	public function department(){
		return $this->belongsTo(Department::class);
	} 

	public function users(){
		return $this->belongsToMany(User::class);
	} 

	public static function findById($department_id){
		return static::where(compact('department_id'))->get();
	} 

	static public function findByStatus($status){
		if ($status == 'All') {
			return static::where('department_id', auth()->user()->department->id)
			->orderby('id','DESC')->get();
		}else{
			return static::where('status', $status)
			->where('department_id', auth()->user()->department->id)
			->orderby('id','DESC')->get();
		}
	}
}
