<?php

namespace App\Notifications;

use App\Ticket;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TicketClosed extends Notification
{
	use Queueable;
	protected $Message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket)
    {
    	$this->ticket = $ticket;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
    	return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
    	return (new MailMessage)
    	->subject()
    	->greeting()
    	->action()
    	->salutation()
    	->line();
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        if($this->ticket->user_id==auth()->user()->id)
            $route = 'tickets.show_admin';
        else
            $route = 'tickets.show';

    	return [
    		'text' => "<b>" . auth()->user()->name . "</b> ha cerrado el ticket #" . $this->ticket->id,
    		'icon' => "<i class='fas fa-window-close'></i>",
    		'picture' => auth()->user()->picture,
    		'route_name' => $route,
    		'id' => $this->ticket->id
    	];
    }
}
