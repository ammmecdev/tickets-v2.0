<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UnreadMessages extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = '/tickets'; 
        $not_name = explode(" " , $notifiable->name);
        return (new MailMessage)
        ->from('sistema_tickets@ammmec.com')
        ->greeting($not_name[0])
        ->subject('Aviso de mensajes pendientes | SISTEMA DE TICKETS')
        ->line('Tienes mensajes sin leer.')
        ->line('No interesa que sigas conectado para darle seguimiento a los asuntos pendientes.')
        ->action('Ir a tickets', url($url))
        ->salutation('Recibe un cordial saludo por parte de TICKETS AMMMEC');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
