<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
		'name', 'type', 'route', 'message_id' 
	];
    
    public function message(){
		return $this->belongsTo(Message::class);
	}
}
