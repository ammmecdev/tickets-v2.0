<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;

class NotificationsController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		return view('notifications.index');
	}

	public function getNotifications()
	{

		return auth()->user()->notifications()->paginate(20);
	}

	public function getUnreadNotifications()
	{
		return auth()->user()->unreadNotifications()->paginate(10);
	}

	public function getNotificationsCount()
	{
		return auth()->user()->unreadNotifications->count();
	}

	public function read(Request $request)
	{
		$notifications = auth()->user()->unreadNotifications;
		foreach($notifications as $index => $notification){
			if($notification->data['id'] == $request->ticket_id){
				DatabaseNotification::find($notification->id)->markAsRead();
			}else{
				$notifications->pull($index);
			}
		}
		$data = [];
        $data['notifications_read'] = $notifications;
        return $data;
	}

	public function destroy($id)
	{
		DatabaseNotification::find($id)->delete();
		return back()->with('flash', 'Notificacion eliminada');
	}
}
