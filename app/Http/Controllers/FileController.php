<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\File;

class FileController extends Controller
{
    //  public function downloadFile($archivo){
        
    //     $public_path = public_path();
    //     $url = $public_path.$archivo;
    //     //verificamos si el archivo existe y lo retornamos
    //     if (\Storage::exists($archivo))
    //     {
    //     return response()->download($url);
    //     }
    //     //si no se encuentra lanzamos un error 404.
    //     abort(404);
    // }

    public function downloadFile(File $file)
    {
        $pathtoFile = public_path().'/'.$file->route;
        return response()->download($pathtoFile);
    }
}
