<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{

	function __construct()
	{
		$this->middleware('guest', ['only' => 'showLoginForm']);
	}

    function login()
    {
        $credentials = $this->validate(request(),[
            'email' => 'email|required|string', 
            'password' => 'required|string',
        ],[
            'email.email' => 'Debes ingresar un correo electrónico válido', 
            'email.required' => 'Debes ingresar un correo empresarial',
            'password.required' => 'Debes ingresar una contraseña',
        ]);
        if (Auth::attempt($credentials))
        {
        	return redirect()->route('tickets');
        }
        return back()->withErrors(['email' => trans('auth.failed')])
        ->withInput(request(['email']));
    }   

    public function showLoginForm()
    {
    	return view('auth.login');
    }

    function logout()
    {
    	Auth::logout();
    	return redirect('/');
    }
}
