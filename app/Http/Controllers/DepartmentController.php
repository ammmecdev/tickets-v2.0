<?php

namespace App\Http\Controllers;

use DB;
use App\Department;
use App\Ticket;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Collection as Collection;

class DepartmentController extends Controller
{

	public function __construct()
	{
		$this->middleware('guest', ['only' => 'showLoginForm']);
	}
	
	function index()
	{
		$department = Department::all();
		return $department;
	}
	public function getDepartmentsAdmins()
	{
		$users = User::where('is_admin', 1)->get();
		$ids = collect();
		foreach($users as $user)
			$ids = $ids->concat([$user->department_id]);
		
		$ids = $ids->unique();

		$array = collect();
		foreach ($ids as $id)
			$array = $array->concat([Department::find($id)]);

		return $array;
	}
}