<?php

namespace App\Http\Controllers;

use App\Subject;
use Illuminate\Http\Request;

class SubjectController extends Controller
{
	public function __construct()
	{
		$this->middleware('guest', ['only' => 'showLoginForm']); 
	}
	
	public function getSubjectsByService($service_id)
	{
		return Subject::where('service_id', $service_id)->get();
	}
}
