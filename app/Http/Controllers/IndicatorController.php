<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use DB;

class IndicatorController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

	public function getTicketsProductivity(Request $request)
	{
		$tickets_status = DB::table('tickets')   
            ->select( 
                'tickets.status_admin as status')

            ->whereBetween('tickets.created_at', [$request->start, $request->end])
            ->where('tickets.department_id', $request->department_id)
            ->get();

        return $tickets_status;
	}

    public function getTicketsByServices(Request $request)
    {
        $tickets_services = DB::table('tickets') 
            ->join('services', 'tickets.service_id', '=', 'services.id')  
            ->select(DB::raw('services.name as service, count(*) as total'))

            ->where('tickets.department_id', $request->department_id)
            ->whereBetween('tickets.created_at', [$request->start, $request->end])
            ->groupBy('services.name')
            ->get();
        
        return $tickets_services;
    }

    public function getTicketsBySubjects(Request $request)
    {
       $tickets_subjects = DB::table('tickets')  
            ->select(DB::raw('tickets.subject, count(*) as total'))
            ->where('tickets.department_id', $request->department_id)
            ->whereBetween('tickets.created_at', [$request->start, $request->end])
            ->groupBy('tickets.subject')
            ->get();
        
        return $tickets_subjects;
    }

    public function getTicketsByDepartments(Request $request)
    {

        $tickets_dep = DB::table('tickets')

            ->join('users', 'tickets.user_id', '=', 'users.id')
            ->join('departments', 'users.department_id', '=', 'departments.id')

            ->select(DB::raw('departments.name as departamento, count(IF(tickets.status_admin = "Cerrado", 1, NULL)) as tickets_cerrados, count(IF(tickets.status_admin = "Abierto" OR tickets.status_admin = "Nuevo" OR tickets.status_admin = "Contestado", 1, NULL)) as tickets_abiertos'))
            ->where('tickets.department_id', $request->department_id)
            ->whereBetween('tickets.created_at', [$request->start, $request->end])
            ->groupBy('departments.name')
            ->get();
        
        return $tickets_dep;
    }

    public function getAttentionTime(Request $request)
    {
        $tickets_attentionTime = DB::table('tickets')
            ->join('services', 'tickets.service_id', '=', 'services.id')
           
            ->select(DB::raw('services.name as services, count(*) as tickets, SUM(TIMESTAMPDIFF(DAY, tickets.created_at, tickets.updated_at)) as total_dias'))

            ->where('tickets.department_id', $request->department_id)
            ->whereBetween('tickets.created_at', [$request->start, $request->end])
            ->groupBy('services.name')
            ->get();
        
        return $tickets_attentionTime;
    }

}
