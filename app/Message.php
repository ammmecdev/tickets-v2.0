<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
		'message', 'ticket_id', 'user_id'
	];

	protected $appends = ['user_name', 'user_picture'];

	public function getUserNameAttribute()
	{
		return $this->user()->first(['name'])->name;
	}

	public function getUserPictureAttribute()
	{
		return $this->user()->first(['picture'])->picture;
	}

    public function ticket(){
		return $this->belongsTo(Ticket::class);
	}

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function files()
	{
		return $this->hasMany(File::class);
	}
}
