<?php
namespace App\Traits;
use Jenssegers\Date\Date;

trait DatesTranslator{
	public function getCreatedAt($created_at){
		return new Date($created_at);
	}

	public function getUpdatedAt($updated_at){
		return new Date($updated_at);
	}

	public function getDeletedAt($deleted_at){
		return new Date($deleted_at);
	}
}

?>