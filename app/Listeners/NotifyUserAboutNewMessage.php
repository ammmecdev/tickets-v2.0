<?php

namespace App\Listeners;

use DB;
use App\User;
use App\Ticket;
use App\TicketUser;
use App\Events\NewMessage;
use App\Notifications\MessageSend;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class NotifyUserAboutNewMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendMessage  $event
     * @return void
     */
    public function handle(NewMessage $event)
    {
        $message = $event->message;
        $ticket = $event->ticket;
        $users = $ticket->users;

        if(auth()->user()->id == $ticket->user_id){
            if($users->isNotEmpty()){
                foreach($users as $user){
                    $user_not = TicketUser::where('user_id', $user->id)->where('ticket_id', $ticket->id)->first();
                    $user_not->messages_unread = $user_not->messages_unread + 1;
                    $success = $user_not->save();
                }  
            }
        }else{
            $ticket->messages_unread = $ticket->messages_unread + 1;
            $ticket->save();
            if($users->isNotEmpty()){
                foreach($users as $index => $user)
                {
                    if($user->id == $message->user_id)
                        $users->pull($index);
                }
            }
            $users->push($ticket->user);
        }
        Notification::send($users, new MessageSend($event->message));   
    }
}
