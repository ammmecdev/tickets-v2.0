<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'employee_number', 'department_id', 'picture', 'is_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'is_admin' => 'boolean'
    ];

    static public function findById($id)
    {
        return static::where(compact('id'))->first();
    }

    public static function findByEmail($email){
        return static::where(compact('email'))->first();
    } 

    public function isAdmin()
    {
        return $this->is_admin;
    }

    public function department(){
        return $this->belongsTo(Department::class);
    } 

    public function messages(){
        return $this->hasMany(Message::class);
    }

    public function services(){
        return $this->belongsToMany(Service::class);
    }

    public function tickets(){
        return $this->belongsToMany(Ticket::class);
    }

      public function tickets_user_limit(){
        return $this->belongsToMany(Ticket::class)->orderby('id','DESC')->limit(5);
    }

    public function tickets_limit()
    {
        return $this->hasMany(Ticket::class)->orderby('id','DESC')->limit(5);
    }  

    public function tickets_all()
    {
        return $this->hasMany(Ticket::class)->orderby('id','DESC');
    } 

    public static function findByStatus($filter)
    {
        $user = $filter == "Admin" ? 1 : ($filter == "Regular" ? 0 : "");
        return static::where('is_admin', 'like', '%' . $user . '%')->orderby('id','ASC')->get();
    }  
}
